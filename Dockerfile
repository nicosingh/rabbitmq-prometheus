FROM prom/prometheus:latest

MAINTAINER Nicolas Singh <nicolas.singh@gmail.com>

COPY prometheus.yml /etc/prometheus
